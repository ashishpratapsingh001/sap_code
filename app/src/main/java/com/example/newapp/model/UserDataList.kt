package com.example.newapp.model


data class User(val name: String, val description: String, val image: String)