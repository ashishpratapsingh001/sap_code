package com.example.newapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.example.newapp.R

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val firstText: TextView = findViewById(R.id.tv1);
        firstText.setOnClickListener(this)
        val secondText: TextView = findViewById(R.id.tv2);
        secondText.setOnClickListener(this)

    }

    override fun onClick(viewId: View?) {
        when (viewId?.id) {
            R.id.tv1, R.id.tv2 -> {
                val intent = Intent(this, UserListActivity::class.java)
                startActivity(intent)
            }

        }

    }
}