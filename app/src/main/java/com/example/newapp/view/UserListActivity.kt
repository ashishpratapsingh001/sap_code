package com.example.newapp.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.newapp.R
import com.example.newapp.model.User
import com.example.newapp.network.ResultFailureUserDataList
import com.example.newapp.network.ResultSuccessWithUserDataList
import com.example.newapp.viewmodel.MainAcitivityViewModel


class UserListActivity : AppCompatActivity() {

    private lateinit var userRecyclerView: RecyclerView
    private val userListActivityAdapter by lazy { UserListActivityAdapter() }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)
        initView();
        initViewModel()

    }

    private fun initViewModel() {

        val userViewModel = ViewModelProvider(this)[MainAcitivityViewModel::class.java]
        userViewModel.getUserDataListResult().observe(this, Observer {
            if (it is ResultFailureUserDataList) {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
            } else if (it is ResultSuccessWithUserDataList) {
                val userDataList = it.userDataList
                userListActivityAdapter.setAndUpdateData(userDataList)
            }

        })

        userViewModel.getUserListData()

    }

    private fun initView() {
        userRecyclerView = findViewById(R.id.userRecyclerView);
        userRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        userRecyclerView.adapter = userListActivityAdapter


        val clickListener: (user: User) -> (Unit) = {


            val fragmentManager = supportFragmentManager
            val transaction = fragmentManager.beginTransaction()
            val fragment = UserDetailFragment()
            val bundle = Bundle()
            bundle.putString("userName", it.name)
            bundle.putString("url", it.image)
            fragment.arguments = bundle
            transaction.replace(R.id.container, fragment,"myfragmnt")
            transaction.addToBackStack(null)
            transaction.commit()


        }
        userListActivityAdapter.setListener(clickListener)

    }

    override fun onResume() {
        super.onResume()
    }
}