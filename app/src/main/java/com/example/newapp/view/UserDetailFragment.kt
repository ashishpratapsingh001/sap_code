package com.example.newapp.view

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.newapp.R

class UserDetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val itemVIew =
            LayoutInflater.from(context).inflate(R.layout.user_detail_page, container, false)
        val userImg: ImageView = itemVIew.findViewById(R.id.userImg)
        val userName: TextView = itemVIew.findViewById(R.id.userName)
        val name = arguments?.getString("userName")
        val url = arguments?.getString("url")

        if (!TextUtils.isEmpty(name)) {
            userName.text = name
        }

        if (!TextUtils.isEmpty(url)) {
            Glide.with(itemVIew.context).load(url).into(userImg)
        }

        return itemVIew
    }

}