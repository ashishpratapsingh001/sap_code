package com.example.newapp.view

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.newapp.R
import com.example.newapp.model.User

class UserListActivityAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val userList = ArrayList<User>()
    private lateinit var clickListener: (user: User) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_user_list_item, parent, false)
        return UserViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is UserViewHolder) {
            holder.bindUI(userList, position, clickListener)
        }
    }

    override fun getItemCount(): Int {

        return userList.size
    }

    fun setAndUpdateData(userList: List<User>) {

        this.userList.clear()
        this.userList.addAll(userList)
        notifyDataSetChanged()

    }

    fun setListener(clickListener: (user: User) -> Unit) {
        this.clickListener = clickListener
    }
}

class UserViewHolder(private val itemView: View) :
    RecyclerView.ViewHolder(itemView) {

    private var userImg: ImageView = itemView.findViewById(R.id.userImg)
    private var userName: TextView = itemView.findViewById(R.id.userName)
    private var userDescription: TextView = itemView.findViewById(R.id.description)

    fun bindUI(userList: List<User>, position: Int,  clickListener: (user: User) -> Unit) {
        itemView.setOnClickListener {
            clickListener(userList[position])
        }
        val user = userList[position]
        if (!TextUtils.isEmpty(user.name)) {
            userName.text = user.name
        }
        if (!TextUtils.isEmpty(user.description)) {
            userDescription.text = user.description
        }
        if (!TextUtils.isEmpty(user.image)) {
            Glide.with(itemView.context).load(user.image).into(userImg)
        }

    }


}
