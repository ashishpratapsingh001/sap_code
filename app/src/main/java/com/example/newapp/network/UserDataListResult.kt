package com.example.newapp.network

import com.example.newapp.model.User


sealed class UserDataListResult
class ResultSuccessWithoutUserDataList(val resultcode: Int) :
    UserDataListResult()

class ResultSuccessWithUserDataList(val resultcode: Int, val userDataList: List<User>) :
    UserDataListResult()

class ResultFailureUserDataList(val resultcode: Int) : UserDataListResult()
