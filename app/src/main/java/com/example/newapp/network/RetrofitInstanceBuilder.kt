package com.example.newapp.network

import retrofit2.GsonConverterFactory
import retrofit2.Retrofit

object RetrofitInstanceBuilder {

    const val BASE_URL = "https://mocki.io/v1/"
    private var retrofit: Retrofit? = null

    fun getRetrofitInstance(): Retrofit? {


        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit

    }

}