package com.example.newapp.network

import com.example.newapp.model.User
import retrofit2.Response
import retrofit2.http.GET


interface UserDataListApi {

    @GET("99066355-8f5e-4c9d-b400-d5bdf26911b6")
    suspend fun getUserListData(): Response<List<User>>
}