package com.example.newapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.newapp.network.UserDataListResult
import com.example.newapp.repo.UserDataListRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainAcitivityViewModel : ViewModel() {
    var userDataListRepo: UserDataListRepo

    init {
        userDataListRepo = UserDataListRepo()
    }

    private val userDataListResult = userDataListRepo.getUserDataListResult()

    fun getUserListData() {

      viewModelScope.launch {

          withContext(Dispatchers.IO){

              userDataListRepo.getUserListData()
          }
      }

    }

    fun getUserDataListResult(): LiveData<UserDataListResult> {
        return userDataListResult
    }
}