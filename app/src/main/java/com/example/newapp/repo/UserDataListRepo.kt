package com.example.newapp.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.newapp.network.*
import java.lang.Exception

class UserDataListRepo {

    private val userDataListResult = MutableLiveData<UserDataListResult>()

    suspend fun getUserListData() {

        val userApiInterface =
            RetrofitInstanceBuilder.getRetrofitInstance()?.create(UserDataListApi::class.java)

        try {
            val userDataListResultResponse = userApiInterface?.getUserListData()
            if (userDataListResultResponse != null && userDataListResultResponse.isSuccessful) {
                if (userDataListResultResponse.body() != null && !userDataListResultResponse.body()!!
                        .isNullOrEmpty()
                ) {

                    userDataListResult.postValue(
                        ResultSuccessWithUserDataList(
                            200,
                            userDataListResultResponse.body()!!
                        )
                    )

                } else {
                    userDataListResult.postValue(ResultSuccessWithoutUserDataList(200))
                }


            } else {

                userDataListResult.postValue(ResultFailureUserDataList(401))
            }
        } catch (e: Exception) {
            userDataListResult.postValue(ResultFailureUserDataList(401))
        }
    }

    fun getUserDataListResult(): LiveData<UserDataListResult> {
        return userDataListResult
    }

}